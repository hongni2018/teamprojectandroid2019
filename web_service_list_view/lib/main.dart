import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

void main() => runApp(MyApp());

//
//// #docregion MyApp
class MyApp extends StatelessWidget {
  // #docregion build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Currencies List',
      home: MainFetchData(),
    );
  }
}
//// #enddocregion build
//}
//
//// #enddocregion MyApp
//
//// #docregion RWS-var

class MainFetchData extends StatefulWidget {
  @override
  _MainFetchDataState createState() => _MainFetchDataState();
}

class _MainFetchDataState extends State<MainFetchData> {
  List<String> list = List();
  final _biggerFont = const TextStyle(fontSize: 18.0);
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("http://openexchangerates.org/api/currencies.json");
    if (response.statusCode == 200) {
      Map<String, dynamic> currencies = convert.jsonDecode(response.body);
      list.clear();
      currencies.forEach(_storeCurrency);
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  _storeCurrency(key, value) {
    list.add(key + " : " + value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Fetch Currencies from Internet"),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
              child: new Text("Fetch Data"),
              color: Theme.of(context).accentColor,
              elevation: 4.0,
              splashColor: Colors.blueGrey,
              textColor: Colors.white,
              onPressed: _fetchData,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0))),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : _notLoading());
  }

  Widget _notLoading() {
    if (list.length > 0) {
      return ListView.separated(
          separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
          padding: const EdgeInsets.all(16.0),
          itemCount: list.length,
          itemBuilder: /*1*/ (context, i) {
            return ListTile(
              title: Text(
                list[i],
                style: _biggerFont,
              ),
            );
          });
    } else {
      return Center(
          child: Text("Please tap \'Fetch\' button to get currencies list"));
    }
  }
}
